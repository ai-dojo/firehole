Firehole is a set of proxies used for vulnerability simulation.

The setup should be as simple as specifying the proxy address, app address, and the vulnerabilities you would like to simulate.
